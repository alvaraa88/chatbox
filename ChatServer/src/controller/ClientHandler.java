package controller;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

//creates a thread for every server.accept client.
public class ClientHandler implements Runnable {
    private Socket clientSocket;
    private ObjectOutputStream toClient;
    private ObjectInputStream fromClient;
    private String user;
    private HashSet<String> topicSet = new HashSet<>();
    public static ArrayList<ClientHandler> clientList = new ArrayList<>();
    private LinkedBlockingQueue<Message> messages = new LinkedBlockingQueue<>();

    public ClientHandler(Socket clientSocket) {
        try {
            this.clientSocket = clientSocket;
            this.toClient = new ObjectOutputStream(clientSocket.getOutputStream());
            this.fromClient = new ObjectInputStream(clientSocket.getInputStream());
            Message getInfo = (Message) fromClient.readObject();
            Message messageToStr = new MessageProtocol(getInfo.getMessage());
            String[] tokens = StringUtils.split(messageToStr.getMessage());
            this.user = tokens[0];
            addWorker(this);
            broadcastOnlineStatus(this);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            closeStreams(clientSocket);
        }
    }
    private void handleClient(Message messageFromClient) throws IOException, InterruptedException {
        if (messageFromClient.getMessage() != null) {
            try {
                messages.add(messageFromClient);
                Message message = messages.take();

                String[] tokens = StringUtils.split(message.getMessage());
                if (tokens != null && tokens.length > 0) {
                    String cmdUser = tokens[0];
                    String cmdKeyWord = tokens[1];
                    if ("logoff".equalsIgnoreCase(cmdKeyWord) || "quit".equalsIgnoreCase(cmdKeyWord) || "exit".equalsIgnoreCase(cmdKeyWord)) {
                        handleOfflineStatus(this);
                    } else if ("login".equalsIgnoreCase(cmdKeyWord)) {
                        handleLogin(tokens);
                    } else if ("join".equalsIgnoreCase(cmdKeyWord)) {
                        handleJoin(tokens);
                        //for every new keyword, create a new function :for modularity.
                    } else if ("leave".equalsIgnoreCase(cmdKeyWord)) {
                        handleLeave(tokens);
                    } else if ("server".equalsIgnoreCase(cmdKeyWord)){
                        Message message1 = new MessageProtocol("Server: response");
                        toClient.writeObject(message1);
                        toClient.flush();
                    } else if ("msg".equalsIgnoreCase(cmdKeyWord)){
                        String[] tokensMsg = StringUtils.split(message.getMessage(), null, 4);
                        handleMessage(tokensMsg);
                    }
                    else {
                        System.out.println("ERROR: " + message.getMessage());
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                clientSocket.close();
                System.out.println("err-clientHandler - closing.");
            }
        }
    }

    private void handleLogin(String[] tokens) throws IOException {
        if (tokens.length == 3) {
            String stringUser = tokens[0];
            String password = tokens[2];
            if (password.equals("password")) {
                this.user = stringUser;

                //add clientHandler to list and makes a thread to "listen"
                //addWorker(this);
                handleOnlineStatus(this);

                //find other online users
                findOthersOnlineStatus(this.getUser());

                //broadcast online status to others
                broadcastOnlineStatus(this);

            } else {
                Message msg = new MessageProtocol("error login\n");
                toClient.writeObject(msg);
                toClient.flush();
                System.err.println("login failed for " + this.getUser());
            }
        }
    }

    //format: "from" "msg" "topic" "body"
    //format: "from" "msg" "directPerson" "body"
    private void handleMessage(String[] tokens) throws IOException {
        String fromName = tokens[0];
        String toName = tokens[2];
        String body = tokens[3];

        boolean isTopic = toName.charAt(0) == '#';

        for (ClientHandler worker : getClientList()) {
            if (isTopic) {
                //group message
                if (worker.isMemberOfTopic(toName)) {
                    worker.send(fromName + ": " + body + "\n");
                }
            } else {
                //direct message
                if (toName.equalsIgnoreCase(worker.getUser())) {
                    worker.send(fromName + ": " + body + "\n");
                }
            }
        }
    }

    private void handleJoin(String[] tokens) throws IOException {
        if (tokens.length > 1) {
            String topic = tokens[2];
            String fromName = tokens[0];
            topicSet.add(topic);
            broadcastAllMessage(fromName + " joined " + topic);
        }

    }

    private void handleLeave(String[] tokens) throws IOException {
        if (tokens.length > 1) {
            String topic = tokens[2];
            String fromName = tokens[0];
            topicSet.remove(topic);
            broadcastAllMessage(fromName + " left " + topic);
        }
    }

    public boolean isMemberOfTopic(String topic) throws IOException {
        //broadcastAllMessage(topic + " is now a topic you can join.");
        return topicSet.contains(topic);
    }

    private void broadcastOnlineStatus(ClientHandler clientHandler) throws IOException {

        for (ClientHandler worker : getClientList()) {
            if (!clientHandler.equals(worker)) {
                worker.send("online " + this.getUser());
            }
        }
    }

    private void findOthersOnlineStatus(String user) throws IOException {

        for (ClientHandler worker : getClientList()) {
            if (user.equals(worker.getUser())) {
                if (worker.getUser() != null) {
                    send("online " + worker.getUser());
                }
            }
        }
    }

    private void broadcastAllMessage(String str) throws IOException {
        for (ClientHandler worker : getClientList()) {
                worker.send(str);
        }
    }

    private void handleOnlineStatus(ClientHandler clientHandler) throws IOException {
        List<ClientHandler> workerList = getClientList();

        for (ClientHandler worker : workerList) {
            if (!clientHandler.equals(worker)) {
                worker.send("online " + worker.getUser());
            }
        }

        clientSocket.close();
    }

    private void handleOfflineStatus(ClientHandler clientHandler) throws IOException {
        removeWorker(this);
        List<ClientHandler> workerList = getClientList();

        for (ClientHandler worker : workerList) {
            if (!clientHandler.equals(worker)) {
                worker.send("offline " + worker.getUser());
            }
        }

        clientSocket.close();
    }

    public String getUser() {
        return user;
    }

    private void send(String strMsg) throws IOException {
        Message message = new MessageProtocol(strMsg);
        if (this.user != null) {
            toClient.writeObject(message);
            toClient.flush();
        }
    }

    public void addWorker(ClientHandler clientHandler) {
        ClientHandler.clientList.add(clientHandler);
    }

    public void removeWorker(ClientHandler clientHandler) {
        clientList.remove(clientHandler);
    }

    public List<ClientHandler> getClientList() {
        return clientList;
    }

    public void closeStreams(Socket socket) {
        try {
            //socket automatically closes out input/output streams
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (clientSocket.isConnected()) {
            try {
                Message messageFromClient;
                //blocking method
                messageFromClient = (Message) fromClient.readObject();
                Message message = new MessageProtocol(messageFromClient.getMessage());
                handleClient(message);

            } catch (IOException | ClassNotFoundException | InterruptedException e) {
                closeStreams(clientSocket);
                break;
            }
        }
    }
}
