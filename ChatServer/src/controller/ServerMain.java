package controller;

import java.util.Scanner;

/**
 * Alexander Alvara
 * Chat Server
 */

public class ServerMain implements Runnable{
    public static int portIDMain;
    public static String addressIDMain;
    public static String serverNameMain;


    @Override
    public void run() {
        Server server = new Server(portIDMain);
        Thread serversThread = new Thread(server, "serverThread");
        //this is the thread connection for communication between all users
        serversThread.start();
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int counter;

        System.out.println("Please enter your IP-Address.");
        addressIDMain = scanner.nextLine();
        //addressIDMain = IPresponse;

        System.out.println("Please enter your Port ID.");
        portIDMain = scanner.nextInt();
        //portIDMain = portResponse;

        counter =+1;
        serverNameMain = "server" + counter;

        ServerMain serverMain = new ServerMain();
        Thread thread = new Thread(serverMain, "ServerMain"+counter);
        thread.start();

    }
}
