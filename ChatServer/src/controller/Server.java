package controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {
    private final int serverPort;
    private Socket clientSocket;
    private ServerSocket serverSocket;
    public Server(int serverPort) {
        this.serverPort = serverPort;
    }
    public void getClientConnection() {
        //need 1 thread to accept client, another for established clients(via ServerWorker).
        try {
            serverSocket = new ServerSocket(serverPort);
            while (!serverSocket.isClosed()) {

                //a blocking line. waiting to accept new clients
                clientSocket = serverSocket.accept();
                System.out.println("connection accepted: " + clientSocket);

                //this class has the input/output streams to talk to mult. clients
                ClientHandler clientHandler = new ClientHandler(clientSocket);

                Thread clientCommThread = new Thread(clientHandler, "clientCommThread");
                clientCommThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
            closeServerSocket();
        }
    }
    public void closeServerSocket() {
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void run() {
        getClientConnection();
    }


}
