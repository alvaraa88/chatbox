package controller;

import java.io.Serializable;
import java.util.Scanner;

public class MessageProtocol implements Message, Serializable {

    public String msg;
    public MessageProtocol(String msg){
        this.msg = msg;
    }

    @Override
    public String getMessage() {
        return msg;
    }

    @Override
    public String writeMessage() {
        Scanner scanner = new Scanner(System.in);
        msg = scanner.nextLine();
        return msg;
    }

    @Override
    public String ToString(){
        return msg;
    }

}
