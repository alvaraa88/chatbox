import controller.Message;
import controller.MessageProtocol;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UnitTestCH {

    @Test
    @DisplayName("Message test.")
    public void testMessage(){

        String expected = "online: alex";
        Message message = new MessageProtocol("online: alex");
        String actualResult= message.getMessage();
        assertEquals(expected, actualResult);
    }

    @ParameterizedTest
    @ValueSource(strings = {"hello", "alex", "online: alex"})
    @DisplayName("Message tests.")
    public void testMessages(String VSinput){

        String expected = "online: alex";
        Message message = new MessageProtocol(VSinput);
        String actualResult= message.getMessage();
        assertEquals(expected, actualResult);
    }

}
