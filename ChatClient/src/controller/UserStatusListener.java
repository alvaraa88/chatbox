package controller;

import controller.Message;

public interface UserStatusListener {
    public void online(Message login);
    public void offline(Message login);
}
