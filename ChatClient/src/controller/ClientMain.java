package controller;

import java.util.Scanner;

public class ClientMain implements Runnable  {
    public static String username;
    public static int serverPort;
    public static String serverAddress;

    @Override
    public void run() {
        Client client = new Client(serverAddress, serverPort);

        Thread clientThread = new Thread(client, "clientThread");
        clientThread.start();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter your name.");
        username = scanner.nextLine();

        System.out.println("Please enter the IP Address");
        serverAddress = scanner.nextLine();

        System.out.println("Please enter the Port ID");
        serverPort = scanner.nextInt();

        ClientMain clientMain = new ClientMain();
        Thread thread = new Thread(clientMain, username + "thread");
        thread.start();
    }
}
