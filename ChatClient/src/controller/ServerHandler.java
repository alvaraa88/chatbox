package controller;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class ServerHandler implements Runnable {
    private Socket clientSocket;
    private String username_str;
    private ObjectOutputStream toServer;
    private ObjectInputStream fromServer;

    public ServerHandler(Socket clientSocket, String username_str) {
        try {
            //this connects us to server
            this.clientSocket = clientSocket;
            this.username_str = username_str;
            this.toServer = new ObjectOutputStream(clientSocket.getOutputStream());
            this.fromServer = new ObjectInputStream(clientSocket.getInputStream());
            sendLogin(username_str);

        } catch (IOException e) {
            e.printStackTrace();
            closeStreams(clientSocket);
        }
    }

    //login takes 3 arguments --> name login password.
    public void sendLogin(String userName) throws IOException {
        Message message;
        String password = "password";
        String loginUser = userName + " " + "login" + " " + password;
        message = new MessageProtocol(loginUser);

        toServer.writeObject(message);
        toServer.flush();
    }

    //autonomous thread for listening to messages.
    public void listenForMessages() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (clientSocket.isConnected()) {
                    try {
                        Message message = (Message) fromServer.readObject();
                        message = new MessageProtocol(message.getMessage());
                        System.out.println(message.getMessage());

                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                        closeStreams(clientSocket);
                    }
                }
            }
        }).start();
    }

    public void sendMessage() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //1.
                    //necessary due to constructor.
//                    Message messageUsername = new MessageProtocol(username_str);
//                    toServer.writeObject(messageUsername);
//                    toServer.flush();

                    //2.
                    while (clientSocket.isConnected()) {
                        System.out.println("Write something.");
                        System.out.println("Message format: " +
                                "(1)msg (2)toPerson/Topic (3)msgBody");
                        System.out.println("Message format: " +
                                "(1)join (2)#Topic");
                        System.out.println("Message format: " +
                                "(1)server");
                        System.out.println("Message format: " +
                                "(1)exit");

                        Scanner scanner = new Scanner(System.in);
                        Message userInput = new MessageProtocol(ClientMain.username + " " + scanner.nextLine());

                        toServer.writeObject(userInput);
                        toServer.flush();

                        String[] tokens2 = StringUtils.split(userInput.getMessage());
                        if (tokens2 != null && tokens2.length > 0) {
                            String cmd2 = tokens2[1];
                            if (userInput != null) {
                                if ("quit".equalsIgnoreCase(cmd2) || "logout".equalsIgnoreCase(cmd2)) {
                                    System.exit(0);
                                }
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    closeStreams(clientSocket);
                }
            }
        }).start();
    }

    public void closeStreams(Socket socket) {
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return ClientMain.username;
    }

    @Override
    public void run() {
        //these 2 methods run indefinitely
        this.listenForMessages();
        this.sendMessage();
    }
}
