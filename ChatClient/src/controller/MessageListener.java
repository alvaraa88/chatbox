package controller;

public interface MessageListener {
    public void onMessage(String user, String msgBody);
}
