package controller;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class Client implements Runnable {
    private Socket socket;
    private int serverPort;
    private String serverAddress;
    private ArrayList<ServerHandler> serverList = new ArrayList<>();
    private Message username_message;
    public Client(String serverAddress, int serverPort) {
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
    }
    public void getServerConnection() throws IOException {
        try {

            //connects client to server.
            if(!connect()){
                System.err.println("connect failed \n");
                socket.close();
            } else {
                String str = ClientMain.username;
                username_message = new MessageProtocol(str);
                System.out.println("Your username is: " + username_message.getMessage());

                //connects client to server via serverHandler
                ServerHandler serverHandler = new ServerHandler(socket, ClientMain.username);
                serverList.add(serverHandler);

                Thread serverCommThread = new Thread(serverHandler, "serverCommThread");
                serverCommThread.start();
            }
        } catch (IOException e){
            e.printStackTrace();
            closeServerSocket();
        }
    }

    public boolean connect(){
        try{
            this.socket = new Socket(serverAddress, serverPort);
            System.out.println("Client port is: " + socket.getPort());
            return true;

        } catch (IOException e){
            e.printStackTrace();
        }
        return false;
    }
    public void closeServerSocket() {
        try {
            //socket automatically closes out input/output streams
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void run() {
        try {
            getServerConnection();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
