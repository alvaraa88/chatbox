# ChatBox

Hello, Thanks for checking out my ChatBox.
Here are some of the main points I would like to express as the objective of this project. 
1) Multi-Channel TCP Communication between user's and servers.
2) Unit testing
3) MVC Framework.

## Getting started:
* The Chatbox consists of 2 distict modules. ChatServer and ChatClient. 
>The **ChatServer** is responsible for authorizing the clients requests and facilitates the communication between clients. 
1 ChatServer must be running for communication between clients to work. Therefore:
1) ChatServer
   1) Must be running before ChatClient.
   2) An IPAddress must be established for the server.
   3) A Port ID must be established for the server.
   4) Both IPAddress and PortID must be known for Client to connect to server.
>The **ChatClient** is for the end-user. Clients can message other clients directly or through topics created. 
> Therefore:
1) ChatClient
   1) Must run after ChatServer has been established.
   2) ChatServers IPAddress and PortID must be known in order to establish a server connection.
   3) Clients can create a topic to message a group.
   4) Clients can directly message an "Online" person.

### Author and website:
1) [Alexander Alvara](https://alexanderalvara.com/resume)
2) [Gitlab](https://gitlab.com/alvaraa88/chatbox)
3) [LinkedIn](https://www.linkedin.com/in/alexander-alvara-4132a3a7/)
